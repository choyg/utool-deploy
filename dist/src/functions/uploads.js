"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const getFirebaseDownloadUrl_1 = require("./getFirebaseDownloadUrl");
const getFilePath_1 = require("./getFilePath");
const getPdfInfo_1 = require("./getPdfInfo");
const dateUtils_1 = require("./dateUtils");
const Resource_1 = require("./Resource");
const getPdfThumbnail_1 = require("./getPdfThumbnail");
const getThumbnailPath_1 = require("./getThumbnailPath");
const getThumbnailFirebaseUrl_1 = require("./getThumbnailFirebaseUrl");
const ulid = require("ulid");
const fs = require("fs");
const firebase = require('firebase');
const database = firebase.database();
const storage = require('@google-cloud/storage');
const gcs = storage({
    projectId: 'signal-mutual-resources',
    keyFilename: 'Signal Mutual Resources-db041140228c.json',
});
const bucket = gcs.bucket('signal-mutual-resources.appspot.com');
function uploadPdfResource(file, title, summary, category, descriptions) {
    let thumbnailUrl;
    let thumbnailFilePath;
    console.log(file);
    getPdfThumbnail_1.default(file.path)
        .then((imageObj) => {
        thumbnailFilePath = imageObj.path;
        return uploadThumbnail(file, thumbnailFilePath);
    })
        .then((thumbnailDownloadUrl) => {
        thumbnailUrl = thumbnailDownloadUrl;
        return uploadToGCS(file);
    })
        .then(() => {
        return getPdfInfo_1.default(file.path);
    })
        .then((meta) => {
        const resource = new Resource_1.default(title, summary, thumbnailUrl, meta.pages, dateUtils_1.dateToMilliseconds(meta.created), category);
        return uploadResourceToDB(file, resource);
    })
        .then((fbPath) => {
        unlinkFile(file.path); // delete uploaded document
        unlinkFile(thumbnailFilePath); // delete thumbnail image
        descriptions.forEach(description => {
            uploadPdfDescription(description, file, category, fbPath);
        });
    })
        .catch((err) => {
        console.log(err);
        unlinkFile(file.path);
        unlinkFile(thumbnailFilePath);
    });
}
exports.uploadPdfResource = uploadPdfResource;
function uploadPdfDescription(description, parentFile, parentCategory, fbPath) {
    const firebasePath = fbPath + "/descriptions";
    console.log(firebasePath);
    uploadToGCS(description.file)
        .then(() => {
        pushDescriptionToFirebase(description.title, description.file, firebasePath).then((message) => {
            console.log(message);
        });
        unlinkFile(description.file.path);
    })
        .catch((err) => {
        console.log(err);
        unlinkFile(description.file.path);
    });
}
exports.uploadPdfDescription = uploadPdfDescription;
/**
 * Pushes given Resource to Firebase realtime database.
 * A unique key from Firebase is generated for each call.
 * @return Promise<string> Path that is created from Firebase push
 */
function pushResourceToFirebase(resource, file) {
    const fileData = {
        title: resource.title,
        summary: resource.summary,
        thumbnail: resource.thumbnail,
        details: resource.details,
        date: resource.date,
        path: getFirebaseDownloadUrl_1.default(file),
        path_size: file.size,
        mimetype: file.mimetype,
    };
    const path = `resources/${resource.category}`;
    const fileRef = database.ref(path);
    return new Promise((resolve, reject) => {
        const key = fileRef.push(fileData, (error) => {
            if (error)
                reject(error);
        }).key;
        if (key)
            resolve(path + '/' + key);
        else
            reject();
    });
}
function pushDescriptionToFirebase(title, file, firebasePath) {
    const fileData = {
        title: title,
        path: getFirebaseDownloadUrl_1.default(file),
    };
    const fileRef = database.ref(firebasePath);
    return new Promise((resolve, reject) => {
        fileRef.push(fileData, (error) => {
            if (error)
                reject(error);
            else
                resolve("successly pushed description");
        });
    });
}
function uploadResourceToDB(file, resource) {
    return pushResourceToFirebase(resource, file);
}
/**
 * Synchronously unlink
 * @param path
 */
function unlinkFile(path) {
    try {
        fs.unlinkSync(path); // delete first pdf page
    }
    catch (e) {
        console.log(e);
    }
}
/**
 * Uploads given file to Google Cloud Storage
 * Uses getFilePath() to generate the storage location
 */
function uploadToGCS(file) {
    const dest = getFilePath_1.default(file);
    const options = {
        destination: dest,
        metadata: {
            contentType: file.mimetype,
            metadata: {
                firebaseStorageDownloadTokens: file.filename,
            },
        },
    };
    return new Promise((resolve, reject) => {
        bucket.upload(file.path, options, (err) => {
            if (err)
                reject(err);
            else
                resolve(dest);
        });
    });
}
function uploadThumbnail(file, thumbnailPath) {
    const dest = getThumbnailPath_1.default(file);
    const uuid = ulid();
    const options = {
        destination: dest,
        metadata: {
            contentType: 'image/jpeg',
            metadata: {
                firebaseStorageDownloadTokens: uuid,
            },
        },
    };
    return new Promise((resolve, reject) => {
        bucket.upload(thumbnailPath, options, (err) => {
            if (err)
                reject(err);
            else
                resolve(getThumbnailFirebaseUrl_1.default(file, uuid));
        });
    });
}
//# sourceMappingURL=uploads.js.map