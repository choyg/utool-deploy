"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const getEncodedPath_1 = require("./getEncodedPath");
function getFirebaseDownloadURL(file) {
    const token = file.filename;
    const path = getEncodedPath_1.default(file);
    return `https://firebasestorage.googleapis.com/v0/b/signal-mutual-resources.appspot.com/o/${path}?alt=media&token=${token}`;
}
exports.default = getFirebaseDownloadURL;
//# sourceMappingURL=getFirebaseDownloadUrl.js.map