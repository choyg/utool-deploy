export default function getFilePath(file: Express.Multer.File): string {
    return `${file.filename}/${file.originalname}`;
}