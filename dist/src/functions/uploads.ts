import getFirebaseDownloadURL from "./getFirebaseDownloadUrl";
import getFilePath from './getFilePath';
import getPdfInfo from './getPdfInfo';
import { dateToMilliseconds } from './dateUtils';
import Resource from './Resource';
import Description from './Description';
import getPdfThumbnail from "./getPdfThumbnail";
import getThumbnailPath from "./getThumbnailPath";
import getThumbnailFirebaseUrl from "./getThumbnailFirebaseUrl";
import * as ulid from 'ulid';
import * as fs from 'fs';

const firebase = require('firebase');

const database = firebase.database();
const storage = require('@google-cloud/storage');
const gcs = storage({
    projectId: 'signal-mutual-resources',
    keyFilename: 'Signal Mutual Resources-db041140228c.json',
});
const bucket = gcs.bucket('signal-mutual-resources.appspot.com');

export function uploadPdfResource(
    file: Express.Multer.File,
    title: string,
    summary: string,
    category: string,
    descriptions: Description[]
) {
    let thumbnailUrl: string;
    let thumbnailFilePath: string;
    getPdfThumbnail(file.path)
        .then((imageObj) => {
            thumbnailFilePath = imageObj.path;
            return uploadThumbnail(file, thumbnailFilePath);
        })
        .then((thumbnailDownloadUrl) => {
            thumbnailUrl = thumbnailDownloadUrl;
            return uploadToGCS(file);
        })
        .then(() => {
            return getPdfInfo(file.path);
        })
        .then((meta) => {
            const resource = new Resource(
                title,
                summary,
                thumbnailUrl,
                meta.pages,
                dateToMilliseconds(meta.created),
                category);
            return uploadResourceToDB(file, resource);
        })
        .then((fbPath) => {
            unlinkFile(file.path); // delete uploaded document
            unlinkFile(thumbnailFilePath); // delete thumbnail image
            descriptions.forEach(description => {
                uploadPdfDescription(description, file, category, fbPath);
            });
        })
        .catch((err) => {
            console.log(err);
            unlinkFile(file.path);
            unlinkFile(thumbnailFilePath);
        })
}

export function uploadPdfDescription(
    description: Description,
    parentFile: Express.Multer.File,
    parentCategory: string,
    fbPath: string
) {
    const firebasePath = fbPath + "/descriptions";
    console.log(firebasePath);
    uploadToGCS(description.file)
        .then(() => {
            pushDescriptionToFirebase(
                description.title,
                description.file,
                firebasePath).then((message) => {
                    console.log(message);
                })
            unlinkFile(description.file.path);
        })
        .catch((err) => {
            console.log(err);
            unlinkFile(description.file.path);
        });
}

/**
 * Pushes given Resource to Firebase realtime database.
 * A unique key from Firebase is generated for each call.
 * @return Promise<string> Path that is created from Firebase push
 */
function pushResourceToFirebase(
    resource: Resource,
    file: Express.Multer.File
): Promise<string> {
    const fileData = {
        title: resource.title,
        summary: resource.summary,
        thumbnail: resource.thumbnail,
        details: resource.details,
        date: resource.date,
        path: getFirebaseDownloadURL(file),
        path_size: file.size,
        mimetype: file.mimetype,
    };
    const path = `resources/${resource.category}`;
    const fileRef = database.ref(path);
    return new Promise((resolve, reject) => {
        const key = fileRef.push(fileData, (error: Error) => {
            if (error) reject(error);
        }).key;
        if (key) resolve(path + '/' + key);
        else reject();
    });
}

function pushDescriptionToFirebase(
    title: string,
    file: Express.Multer.File,
    firebasePath: string
): Promise<string> {
    const fileData = {
        title: title,
        path: getFirebaseDownloadURL(file),
    };
    const fileRef = database.ref(firebasePath);
    return new Promise((resolve, reject) => {
        fileRef.push(fileData, (error: Error) => {
            if (error) reject(error);
            else resolve("successly pushed description");
        });
    });
}

function uploadResourceToDB(
    file: Express.Multer.File,
    resource: Resource
): Promise<any> {
    return pushResourceToFirebase(resource, file);
}

/**
 * Synchronously unlink
 * @param path
 */
function unlinkFile(path: string) {
    try {
        fs.unlinkSync(path) // delete first pdf page
    } catch (e) {
        console.log(e);
    }
}

/**
 * Uploads given file to Google Cloud Storage
 * Uses getFilePath() to generate the storage location
 */
function uploadToGCS(file: Express.Multer.File): Promise<string> {
    const dest = getFilePath(file);
    const options = {
        destination: dest,
        metadata: {
            contentType: file.mimetype,
            metadata: {
                firebaseStorageDownloadTokens: file.filename,
            },
        },
    };

    return new Promise((resolve, reject) => {
        bucket.upload(file.path, options, (err: Error) => {
            if (err) reject(err);
            else resolve(dest)
        })
    });
}

function uploadThumbnail(
    file: Express.Multer.File,
    thumbnailPath: string
): Promise<string> {
    const dest = getThumbnailPath(file);
    const uuid = ulid();
    const options = {
        destination: dest,
        metadata: {
            contentType: 'image/jpeg',
            metadata: {
                firebaseStorageDownloadTokens: uuid,
            },
        },
    };
    return new Promise((resolve, reject) => {
        bucket.upload(thumbnailPath, options, (err: Error) => {
            if (err) reject(err);
            else resolve(getThumbnailFirebaseUrl(file, uuid))
        })
    });
}
