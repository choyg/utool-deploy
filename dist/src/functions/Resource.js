"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Resource {
    constructor(title, summary, thumbnail, details, date, category) {
        this.title = title;
        this.summary = summary;
        this.thumbnail = thumbnail;
        this.details = details;
        this.date = date;
        this.category = category;
    }
}
exports.default = Resource;
//# sourceMappingURL=Resource.js.map