"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const EasyImage = require("easyimage");
const ulid = require("ulid");
const fs = require("fs");
const getPdfFirstPage_1 = require("./getPdfFirstPage");
function getPdfThumbnail(path) {
    let mPdfPath;
    return new Promise((resolve, reject) => {
        getPdfFirstPage_1.default(path)
            .then((pdfPath) => {
            mPdfPath = pdfPath;
            return EasyImage.convert({
                src: pdfPath,
                dst: `./thumbnails/${ulid()}.jpg`,
                quality: 30,
            });
        })
            .then((imgObj) => {
            try {
                fs.unlinkSync(mPdfPath); // delete first pdf page
            }
            catch (e) {
                console.log(e);
            }
            resolve(imgObj);
        })
            .catch((err) => {
            try {
                fs.unlinkSync(mPdfPath); // delete first pdf page
            }
            catch (e) {
                console.log(e);
            }
            reject(err);
        });
    });
}
exports.default = getPdfThumbnail;
//# sourceMappingURL=getPdfThumbnail.js.map