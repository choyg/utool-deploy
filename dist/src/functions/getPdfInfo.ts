const pdfInfo = require('pdfinfo');

export default function getPdfInfo(filePath: string): Promise<any> {
    const pdf = pdfInfo(filePath);
    return new Promise((resolve, reject) => {
        pdf.info((err: any, meta: any) => {
            if (err) reject(err);
            else resolve(meta);
        })
    });
}