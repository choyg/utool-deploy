export default function getEncodedPath(file: Express.Multer.File): string {
    const encodedFilename = encodeURIComponent(file.filename);
    const encodedOriginal = encodeURIComponent(file.originalname);
    return `${encodedFilename}%2F${encodedOriginal}`;
}