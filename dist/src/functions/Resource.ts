class Resource {
    title: string;
    summary: string;
    thumbnail: string;
    details: string;
    date: number;
    category: string;

    constructor(title: string,
                summary: string,
                thumbnail: string,
                details: string,
                date: number,
                category: string) {
        this.title = title;
        this.summary = summary;
        this.thumbnail = thumbnail;
        this.details = details;
        this.date = date;
        this.category = category;
    }
}

export default Resource;