"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getThumbnailPath(file) {
    return file.filename + "/thumbnail.jpg";
}
exports.default = getThumbnailPath;
//# sourceMappingURL=getThumbnailPath.js.map