"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getThumbnailEncodedPath(file) {
    return file.filename + "%2Fthumbnail.jpg";
}
exports.default = getThumbnailEncodedPath;
//# sourceMappingURL=getThumbnailEncodedPath.js.map