"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getEncodedPath(file) {
    const encodedFilename = encodeURIComponent(file.filename);
    const encodedOriginal = encodeURIComponent(file.originalname);
    return `${encodedFilename}%2F${encodedOriginal}`;
}
exports.default = getEncodedPath;
//# sourceMappingURL=getEncodedPath.js.map