"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const pdfInfo = require('pdfinfo');
function getPdfInfo(filePath) {
    const pdf = pdfInfo(filePath);
    return new Promise((resolve, reject) => {
        pdf.info((err, meta) => {
            if (err)
                reject(err);
            else
                resolve(meta);
        });
    });
}
exports.default = getPdfInfo;
//# sourceMappingURL=getPdfInfo.js.map