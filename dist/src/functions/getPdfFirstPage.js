"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Scissors = require("scissors");
const fs = require("fs");
const ulid = require("ulid");
const path = require("path");
/**
 * Takes pdf and returns the first page1
 * @param path Path to given PDF
 * @returns {Promise<string>} resolve: Path to PDF first page
 */
function getPdfFirstpage(path) {
    return new Promise(((resolve, reject) => {
        const key = ulid();
        const pagePath = `./thumbnails/${key}.pdf`;
        ensureDirectoryExistence(pagePath);
        Scissors(path)
            .pages(1)
            .pdfStream()
            .pipe(fs.createWriteStream(pagePath))
            .on('finish', function () {
            resolve(pagePath);
        }).on('error', function (err) {
            unlinkFile(pagePath);
            console.log(err);
            reject(err);
        });
    }));
}
exports.default = getPdfFirstpage;
/**
 * Synchronously unlink
 * @param path
 */
function unlinkFile(path) {
    fs.unlinkSync(path);
}
/**
 * Synchronously create a directory if it does not exist
 */
function ensureDirectoryExistence(filePath) {
    var dirname = path.dirname(filePath);
    if (fs.existsSync(dirname)) {
        return true;
    }
    ensureDirectoryExistence(dirname);
    fs.mkdirSync(dirname);
}
//# sourceMappingURL=getPdfFirstPage.js.map