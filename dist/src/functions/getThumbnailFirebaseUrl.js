"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const getThumbnailEncodedPath_1 = require("./getThumbnailEncodedPath");
function getThumbnailFirebaseUrl(file, token) {
    const path = getThumbnailEncodedPath_1.default(file);
    return `https://firebasestorage.googleapis.com/v0/b/signal-mutual-resources.appspot.com/o/${path}?alt=media&token=${token}`;
}
exports.default = getThumbnailFirebaseUrl;
//# sourceMappingURL=getThumbnailFirebaseUrl.js.map