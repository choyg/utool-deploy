"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getFilePath(file) {
    return `${file.filename}/${file.originalname}`;
}
exports.default = getFilePath;
//# sourceMappingURL=getFilePath.js.map