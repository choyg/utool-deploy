"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getMonthNumber(month) {
    const lowercaseMonth = month.toLowerCase();
    const months = {
        jan: 0,
        feb: 1,
        mar: 2,
        apr: 3,
        may: 4,
        jun: 5,
        jul: 6,
        aug: 7,
        sep: 8,
        oct: 9,
        nov: 10,
        dec: 11,
    };
    return months[lowercaseMonth];
}
exports.getMonthNumber = getMonthNumber;
function dateToMilliseconds(dateString) {
    try {
        const date = dateString.split(' ');
        const year = date[date.length - 1];
        const month = date[1];
        let day;
        if (date[2] === '')
            day = date[3];
        else
            day = date[2];
        const dateObj = new Date(parseInt(year), getMonthNumber(month), parseInt(day), 0, 0, 0, 0);
        console.log(dateObj.getTime());
        return dateObj.getTime();
    }
    catch (error) {
        return new Date().getTime();
    }
}
exports.dateToMilliseconds = dateToMilliseconds;
//# sourceMappingURL=dateUtils.js.map