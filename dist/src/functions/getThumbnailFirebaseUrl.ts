import getThumbnailEncodedPath from "./getThumbnailEncodedPath";

export default function getThumbnailFirebaseUrl(file: Express.Multer.File, token: string) {
    const path = getThumbnailEncodedPath(file);
    return `https://firebasestorage.googleapis.com/v0/b/signal-mutual-resources.appspot.com/o/${path}?alt=media&token=${token}`;
}