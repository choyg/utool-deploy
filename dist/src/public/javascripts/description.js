var descriptionHost = document.getElementById('description-host');
var current;
current = getLastDesc();
current.addEventListener('keyup', descriptionListener, false);
var currentCount = 1;

function getDescriptionDiv() {
    var newDiv = document.createElement("div");
    newDiv.innerHTML = "            <div class=\"form-group form-inline\">\n" +
        "                <label class=\"sr-only\" for=\"inlineFormInput\">Title</label>\n" +
        "                <input type=\"text\" name=\"descTitle" + currentCount + "\" class=\"form-control description mb-2 mr-sm-2 mb-sm-0\" id=\"inlineFormInput\"\n" +
        "                       placeholder=\"Title\">\n" +
        "                <label class=\"sr-only\" for=\"exampleInputFile\">File input</label>\n" +
        "                <input type=\"file\" name=\"description\" class=\"form-control-file\" id=\"exampleInputFile\" aria-describedby=\"fileHelp\">\n" +
        "            </div>";
    currentCount++;
    return newDiv;
}

function descriptionListener() {
    var descDiv = getDescriptionDiv();
    descriptionHost.appendChild(descDiv);
    current.removeEventListener('keyup', descriptionListener, false);
    current = getLastDesc();
    current.addEventListener('keyup', descriptionListener, false);
}

function getLastDesc() {
    var descriptions = document.getElementsByClassName('description');
    return descriptions[descriptions.length - 1];
}