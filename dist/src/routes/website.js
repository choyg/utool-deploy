const express = require('express');
const router = express.Router();
const firebase = require('firebase');

/* GET home page. */
router.get('/', function (req, res, next) {
  var user = firebase.auth().currentUser;
  if (user) {
    res.render('website', {title: 'Add Website'});
  } else {
    res.redirect('/login');
  }
});

router.post('/', function (req, res, next) {
  var user = firebase.auth().currentUser;
  if (!user) {
    res.redirect('/login');
    return;
  }

  var category = req.body.webCategory;
  if (!validateCategory(category)) {
    res.render('website', {title: 'BOO'});
  }
});

function validateCategory(category) {
  switch (category) {
    case 'training':
      return true;
    case 'safety':
      return true;
    case 'policy':
      return true;
    case 'resources':
      return true;
    default:
      return false;
  }
}

module.exports = router;
