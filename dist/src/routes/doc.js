const Uploads = require('../functions/uploads');
const firebase = require('firebase');
const multer = require('multer');
const express = require('express');
const Description = require('../functions/Description').default;

const router = express.Router();
const upload = multer(
  {
    dest: 'uploads/',
    limits: {
      fileSize: 100 * 1024 * 1024, // no larger than 100mb
    },
  },
);

/* GET document upload page. */
router.get('/', (req, res) => {
  const user = firebase.auth().currentUser;
  if (user) {
    return res.render('doc', { title: 'Add Document' });
  }
  return res.redirect('/login');
});

/* POST upload a new document + descriptions */
router.post('/', upload.fields([
  { name: 'document', maxCount: 1 },
  { name: 'description', maxCount: 6 }]), (req, res) => {
    const user = firebase.auth().currentUser;
    if (!user) {
      return res.redirect('/login');
    }

    const docFile = req.files.document[0];
    let descriptions = [];
    if (req.files.description !== undefined) {
      let count = 0;
      req.files.description.forEach((file) => {
        //Create a new Description object and append to array
        let descTitle = req.body[`descTitle${count}`];
        if (!descTitle) {
          descTitle = file.originalname;
        }
        descriptions.push(new Description(descTitle, file));
        count += 1;
      });
    }
    Uploads.uploadPdfResource(
      docFile,
      req.body.docTitle,
      req.body.docSummary,
      req.body.docCategory,
      descriptions);

    return res.redirect('/document');
  });

module.exports = router;
