var express = require('express');
var router = express.Router();
var firebase = require("firebase");

router.get('/', function (req, res, next) {
    firebase.auth().signOut();
    res.redirect('/');
});

router.post('/', function (req, res, next) {
    firebase.auth().signOut()
});

module.exports = router;