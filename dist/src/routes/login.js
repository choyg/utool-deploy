var express = require('express');
var router = express.Router();
var firebase = require("firebase");

router.get('/', function (req, res, next) {
    var user = firebase.auth().currentUser;
    if (user) {
        res.redirect('/document');
    } else {
        res.render('login', {error: ""});
    }
});

router.post('/', function (req, res, next) {
    var email = req.body.email;
    var password = req.body.password;
    firebase.auth().signInWithEmailAndPassword(email, password)
        .then(function (user) {
            res.redirect('/document'); //TODO don't commit
        })
        .catch(function (error) {
            var errorCode = error.code;
            var errorMessage = "";
            if (errorCode === 'auth/wrong-password') {
                errorMessage = "Wrong password"
            } else {
                errorMessage = "Could not login, try again."
            }
            res.render('login', {error: errorMessage});
        });
});

module.exports = router;
